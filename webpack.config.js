const path = require("path");

module.exports = {
  mode: "production", // "production" | "development" | "none"
  entry: "./src/pubsub.js",
  resolve: {
    extensions: [".js"],
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "twitch.pubsub.min.js",
  },
};
