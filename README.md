# AnotherTwitchPubSub

![GitHub release (latest by date including pre-releases)](https://img.shields.io/github/v/release/xogumon/twitchPubSub?color=blueviolet&include_prereleases) ![Minified file size in bytes](https://img.shields.io/github/size/xogumon/twitchPubSub/dist/twitch.pubsub.min.js) ![GitHub](https://img.shields.io/github/license/xogumon/twitchPubSub)

Just another Twitch PubSub helper library.

It's a simple library that allows you to easily subscribe to Twitch PubSub channels and receive messages from them in real time.

### CDN Links

- jsDelivr: [https://cdn.jsdelivr.net/gh/xogumon/twitchPubSub@latest/dist/twitch.pubsub.min.js](https://cdn.jsdelivr.net/gh/xogumon/twitchPubSub@latest/dist/twitch.pubsub.min.js)

### Installation

```html
<script src="https://cdn.jsdelivr.net/gh/xogumon/twitchPubSub@latest/dist/twitch.pubsub.min.js"></script>
<script>
  const pubsub = new AnotherTwitchPubSub({
    channelId: "147018336", // Channel ID
    authToken: "cfabdegwdoklmawdzdo98xt2fo512y", // You can get this from the Twitch API
    topics: ["channel-bits-events-v2", "channel-points-channel-v1"], // You can found all the topics in the Twitch PubSub API documentation (https://dev.twitch.tv/docs/pubsub)
    reconnect: true, // reconnect to Twitch PubSub if connection is lost (default: true)
    autoConnect: true, // automatically connect to Twitch PubSub (default: true)
  });
</script>
```

You don't need include the channel ID in the topics array. It will be automatically added.

### Available topics

- List of available topics can be found [here](https://dev.twitch.tv/docs/pubsub/#available-topics).

### Usage (JavaScript)

```js
const pubsub = new AnotherTwitchPubSub({
  channelId: "<YOUR CHANNEL ID>", // Channel ID (required)
  authToken: "<YOUR AUTH TOKEN>", // You can get this from the Twitch API (required)
  topics: ["channel-bits-events-v2", "channel-points-channel-v1"], // You can subscribe to multiple topics at once (default: [])
  reconnect: true, // automatically reconnect to Twitch PubSub if connection is lost (default: true) - added in v0.1.1
  autoConnect: true, // automatically connect to Twitch PubSub (default: true) - added in v0.1.1
});

pubsub.on("open", () => {
  // Connection is open
  console.log("Connected to pubsub");
});

pubsub.on("close", () => {
  // Connection is closed
  console.log("Disconnected from pubsub");
});

pubsub.on("error", (error) => {
  // Handle error
  console.log(error);
});

pubsub.on("ping", () => {
  // Make sure you're connected to pubsub
  console.log("PING?");
});

pubsub.on("pong", () => {
  // Response to ping from pubsub
  console.log("PONG!");
});

pubsub.on("message", (data) => {
  // raw message data
  console.log(data);
});

pubsub.on("reward-redeemed", (data) => {
  // reward-redeemed data
  console.log(data);
  /* Response
  {
      "id": "9a52b112-1fc5-4feb-bf2c-63c5f871c2c4",
      "user": {
          "id": "147018336",
          "login": "xogum",
          "display_name": "Xogum"
      },
      "channel_id": "147018336",
      "redeemed_at": "2022-08-06T23:46:37.343527034Z",
      "reward": {
          "id": "5948e43f-8506-4eb9-a4d4-4a63a4022a88",
          "channel_id": "147018336",
          "title": "MICO",
          "prompt": "",
          "cost": 250,
          "is_user_input_required": false,
          "is_sub_only": false,
          "image": {
              "url_1x": "https://static-cdn.jtvnw.net/custom-reward-images/147018336/5948e43f-8506-4eb9-a4d4-4a63a4022a88/8445a5f6-c95d-4bf2-ad3d-d48eddc501fb/custom-1.png",
              "url_2x": "https://static-cdn.jtvnw.net/custom-reward-images/147018336/5948e43f-8506-4eb9-a4d4-4a63a4022a88/8445a5f6-c95d-4bf2-ad3d-d48eddc501fb/custom-2.png",
              "url_4x": "https://static-cdn.jtvnw.net/custom-reward-images/147018336/5948e43f-8506-4eb9-a4d4-4a63a4022a88/8445a5f6-c95d-4bf2-ad3d-d48eddc501fb/custom-4.png"
          },
          "default_image": {
              "url_1x": "https://static-cdn.jtvnw.net/custom-reward-images/default-1.png",
              "url_2x": "https://static-cdn.jtvnw.net/custom-reward-images/default-2.png",
              "url_4x": "https://static-cdn.jtvnw.net/custom-reward-images/default-4.png"
          },
          "background_color": "#9B00F5",
          "is_enabled": true,
          "is_paused": false,
          "is_in_stock": false,
          "max_per_stream": {
              "is_enabled": false,
              "max_per_stream": 0
          },
          "should_redemptions_skip_request_queue": true,
          "template_id": null,
          "updated_for_indicator_at": "2022-07-18T15:10:45.438309683Z",
          "max_per_user_per_stream": {
              "is_enabled": false,
              "max_per_user_per_stream": 0
          },
          "global_cooldown": {
              "is_enabled": true,
              "global_cooldown_seconds": 60
          },
          "redemptions_redeemed_current_stream": null,
          "cooldown_expires_at": "2022-08-06T23:47:37Z"
      },
      "status": "FULFILLED"
  }
  */
});

/* Added in 0.1.1 */
pubsub.state(); // returns the current state of the connection (open, closed, connecting, disconnected)

pubsub.connect(); // manually connect to pubsub

pubsub.reconnect(); // Reconnect to pubsub if connection is lost

pubsub.subscribe(["<String||Array>"]); // Subscribe to topics (String or Array)

pubsub.unsubscribe(["<String||Array>"]); // Unsubscribe from topics (String or Array)

pubsub.disconnect(); // Disconnect from pubsub

pubsub.subscribedTopics(); // Get all subscribed topics (Array)

pubsub.subscribedTopicsCount(); // Get number of subscribed topics (Number)

pubsub.isSubscribed("<String>"); // Check if subscribed to topic (returns true or false)
```

### Available events (alphabetical order)

- AUTOMOD-QUEUE
- CHANNEL-BITS-BADGE-UNLOCKS
- CHANNEL-BITS-EVENTS-V2
- CHANNEL-POINTS-CHANNEL-V1
- CHANNEL-SUBSCRIBE-EVENTS-V1
- CHAT-MODERATOR-ACTIONS
- CLOSE
- CONNECT
- DISCONNECT
- ERROR
- LISTEN
- MESSAGE
- OPEN
- PING
- PONG
- RECONNECT
- RESPONSE
- REWARD-REDEEMED _(added in 0.1.1)_
- UNLISTEN
- USER-MODERATION-NOTIFICATIONS
- WHISPERS

_Event names are case-insensitive_

### Token generation

You can generate a token here:
`https://token.decicus.com/`

### Twitch PubSub documentation

A Twitch PubSub documentation is available [here](https://dev.twitch.tv/docs/pubsub/).
