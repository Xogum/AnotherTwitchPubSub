# Changelog

## [0.1.0] (2022-08-06)

- First stable release.

## [0.1.1] (2022-08-06)

- Added connect, reconnect, state, subscribedTopics, subscribedTopicsCount, isSubscribed, and disconnect methods.
- Added autoConnect and reconnect options to constructor.
- Added REWARD-REDEEMED event.
