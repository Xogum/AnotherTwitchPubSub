/*
@class AnotherTwitchPubSub
@constructor
@param {Object} options The options for the application.
@param {String} options.channelId The channel id to subscribe to.
@param {String} options.authToken The authentication token to use.
@param {Boolean} options.autoConnect Whether to connect automatically.
@param {Boolean} options.reconnect Whether to reconnect on disconnect.
@param {Array} options.topics The topics to subscribe to.
*/
class AnotherTwitchPubSub {
  constructor(options) {
    this._client = null; // set the client to null
    this._events = {}; // set the events to an empty object
    this._heartbeatHandler; // set the heartbeat handler to null
    this._options = options; // set the options
    this._options.autoConnect = this._options.autoConnect || true; // set the auto connect option to true if it doesn't exist
    this._options.reconnect = this._options.reconnect || true; // set the reconnect option to true if it doesn't exist
    this._options.topics = this._options.topics || []; // set the topics to the options or an empty array
    this._pubsub = "wss://pubsub-edge.twitch.tv"; // set the pubsub endpoint
    this._reconnectAttempts = 0; // set the reconnect attempts to 0
    if (this._options.channelId && this._options.authToken) {
      // if the channel id and auth token exist
      if (this._options.autoConnect) this._connect(); // connect to the pubsub endpoint
    } else {
      // otherwise
      throw new Error("Channel id and auth token are required"); // throw an error
    }
  }

  _nonce() {
    // generate a random nonce
    let response = ""; // set the response to an empty string
    const possible =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"; // set the possible characters
    for (let i = 0; i < 24; i++) {
      // for each character in the nonce
      response += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return response; // return the nonce
  }

  _slug(string, separator) {
    // slugify a string
    if (typeof string !== "string") {
      return ""; // Return an empty string if the input is not a string
    }
    if (typeof separator !== "string") {
      separator = "-"; // default to hyphen
    }
    const slug = string.toLowerCase().replace(/[^a-z0-9]/g, separator); // replace all non-alphanumeric characters with the separator
    if (separator.length > 1) {
      slug = slug
        .replace(new RegExp(`${separator}{2,}`, "g"), separator) // replace multiple separators with a single one
        .replace(new RegExp(`^${separator}|${separator}$`, "g"), ""); // remove leading and trailing separators
    }
    return slug; // return the slug
  }

  _emit(event, ...args) {
    // emit an event
    if (typeof event === "string") {
      // if the event is a string
      event = this._slug(event); // slugify the event name
      this._events[event] = this._events[event] || []; // create an array for the event if it doesn't exist
      this._events[event].forEach((callback) => {
        // for each callback for the event
        callback(...args); // call the callback with the arguments
      });
    } else {
      throw new Error("Event name must be a string"); // throw an error if the event is not a string
    }
  }

  _connect() {
    // connect to the pubsub endpoint
    this._client = new WebSocket(this._pubsub); // create a new websocket
    this._client.onopen = this._onOpen.bind(this); // bind the onopen event
    this._client.onmessage = this._onMessage.bind(this); // bind the onmessage event
    this._client.onerror = this._onError.bind(this); // bind the onerror event
    this._client.onclose = this._onClose.bind(this); // bind the onclose event
    if (this.state() === "open" || this.state() === "connecting") {
      this._reconnectAttempts = 0; // set the reconnect attempts to 0
      this._emit("connect"); // emit a connect event
    } else {
      this._reconnectAttempts++; // increment the reconnect attempts
      setTimeout(this._connect.bind(this), 1000); // try to connect again in 1 second
    }
  }

  _heartbeat() {
    // heartbeat function
    this._emit("ping"); // emit a ping event
    this._send({
      type: "PING", // send a ping message
    });
    this._heartbeatHandler = setTimeout(this._heartbeat.bind(this), 6e4); // set a timeout to send a ping message every 60 seconds
  }

  _onOpen() {
    // onopen event
    this._emit("open"); // emit an open event
    this._heartbeat(); // start the heartbeat
    this.subscribe(this._options.topics); // subscribe to the topics
  }

  _onMessage(event) {
    // onmessage event
    const data = JSON.parse(event.data); // parse the data
    switch (data.type) {
      case "MESSAGE": // if the message type is a message
        this._emit("message", data.data); // emit a message event with the data
        const topic = data.data?.topic?.split(".")?.find((e) => e);
        const message = JSON.parse(data.data?.message ?? "{}");
        if (topic && message && message.data) {
          // if the topic and message are not null
          this._emit(topic, message); // emit the topic event with the message
          if (
            topic === "channel-points-channel-v1" &&
            message.data.hasOwnProperty("redemption")
          ) {
            // if the topic is channel-points-channel-v1 and the message has a redemption property
            this._emit(message.type, message.data.redemption); // emit the redemption event with the redemption
          }
        }
        break;
      case "RESPONSE": // if the message type is a response
        this._emit("response", data); // emit a response event with the data
        if (data.error) {
          switch (
            data.error // switch on the error
          ) {
            case "ERR_BADAUTH": // if the error is ERR_BADAUTH
              this._onError("Invalid authentication token");
              break;
            case "ERR_BADTOPIC": // if the error is ERR_BADTOPIC
              this._onError("Invalid topic");
              break;
            case "ERR_BADMESSAGE": // if the error is ERR_BADMESSAGE
              this._onError("Invalid message");
              break;
            case "ERR_SERVER": // if the error is ERR_SERVER
              this._onError("Server error");
              break;
            default: // if the error is not one of the above
              // emit an error event with the error
              if (data.error?.length) this._onError(data.error);
              break;
          }
        }
        break;
      case "LISTEN": // if the message type is a listen
        this._emit("listen", data); // emit a listen event with the data
        break;
      case "RECONNECT": // if the message type is a reconnect
        this.reconnect(); // re-connect to the pubsub endpoint
        break;
      case "PONG": // if the message type is a pong
        this._emit("pong"); // emit a pong event
        break;
      case "INVALIDATE": // if the message type is an invalidate
        this._emit("invalidate", data); // emit an invalidate event with the data
        break;
      case "DISCONNECT": // if the message type is a disconnect
        this._emit("disconnect", data); // emit a disconnect event with the data
        break;
      case "UNLISTEN": // if the message type is an unlisten
        this._emit("unlisten", data); // emit an unlisten event with the data
        break;
      default: // if the message type is not one of the above
        // emit an unknown event with the data
        this._emit("unknown", data);
        break;
    }
  }

  _onError(error) {
    // onerror event
    this._emit("error", error); // emit an error event with the error
  }

  _onClose(event) {
    // onclose event
    clearInterval(this._heartbeatHandler); // clear the heartbeat handler
    if (this._options.reconnect) {
      this.reconnect(); // re-connect to the pubsub endpoint
    }
    this._emit("close", event); // emit a close event with the event
  }

  _send(data) {
    // send data to the pubsub endpoint
    if (this.state() === "open") {
      // if the client is open
      this._client.send(JSON.stringify(data)); // send the data
    } else {
      // if the client is not open
      this._onError("WebSocket is not open"); // emit an error event with the error
    }
  }

  on(event, callback) {
    // on event
    if (typeof event === "string") {
      // if the event is a string
      event = this._slug(event); // slugify the event name
      this._events[event] = this._events[event] || []; // create an array for the event if it doesn't exist
      this._events[event].push(callback); // push the callback to the array for the event
    } else {
      // if the event is not a string
      throw new Error("Event name must be a string"); // throw an error if the event is not a string
    }
  }

  subscribe(topics) {
    // subscribe to the topics
    if (typeof topics === "string") {
      topics = [topics]; // set the topics to an array if it's a string
    } else if (!Array.isArray(topics)) {
      throw new Error("Topics must be a string or an array"); // throw an error if the topics are not an array or a string
    }
    if (topics.length) {
      // if the topics are not empty
      // add the topics to the options
      this._options.topics = [...this._options.topics, ...topics];
      // subscribe to a topic
      this._send({
        type: "LISTEN", // send a listen message
        nonce: this._nonce(), // generate a random nonce
        data: {
          topics: topics.map((topic) => {
            return `${topic}.${this._options.channelId}`;
          }), // send the topics to subscribe
          auth_token: this._options.authToken, // send the authentication token
        },
      });
    }
  }

  unsubscribe(topics) {
    // unsubscribe from the topics
    if (typeof topics === "string") {
      topics = [topics]; // set the topics to an array if it's a string
    } else if (!Array.isArray(topics)) {
      throw new Error("Topics must be a string or an array"); // throw an error if the topics are not an array or a string
    }
    if (topics.length) {
      // if the topics are not empty
      // remove the topics from the options
      this._options.topics = this._options.topics.filter(
        (e) => !topics.includes(e)
      );
      // unsubscribe from a topic
      this._send({
        type: "UNLISTEN", // send an unlisten message
        nonce: this._nonce(), // generate a random nonce
        data: {
          topics: topics.map((topic) => {
            return `${topic}.${this._options.channelId}`;
          }), // send the topics to unsubscribe
          auth_token: this._options.authToken, // send the authentication token
        },
      });
    }
  }

  connect() {
    // manually connect to the pubsub endpoint
    this._connect();
  }

  reconnect() {
    // reconnect to the pubsub endpoint
    if (this.state() === "open" || this.state() === "connecting") {
      this._client.close(); // close the client
    }
    this._emit("reconnect"); // emit a reconnect event
    this._connect(); // connect to the pubsub endpoint
  }

  disconnect() {
    // disconnect from the pubsub endpoint
    if (this.state() === "open" || this.state() === "connecting") {
      this._client.close(); // close the client
    }
    this._emit("disconnect"); // emit a disconnect event
  }

  subscribedTopics() {
    // get the topics
    if (Array.isArray(this._options.topics)) {
      return this._options.topics; // return the topics
    } else {
      return []; // return an empty array
    }
  }

  subscribedTopicsCount() {
    // get the topics count
    if (Array.isArray(this._options.topics)) {
      return this._options.topics.length; // return the topics count
    } else {
      return 0; // return 0 if the topics are not an array
    }
  }

  isSubscribed(topic) {
    // find the topic
    if (Array.isArray(this._options.topics)) {
      return this._options.topics.find(
        (e) => e.toLowerCase() === topic.toLowerCase()
      ); // return true if the topic is in the topics array
    } else {
      return false; // return false if the topics are not an array
    }
  }

  state() {
    // get the connection status
    if (this._client) {
      // if the client is open
      switch (this._client.readyState) {
        case WebSocket.CONNECTING: // if the client is connecting
          return "connecting"; // return the connecting state
        case WebSocket.OPEN: // if the client is open
          return "open"; // return the open state
        case WebSocket.CLOSING: // if the client is closing
          return "closing"; // return the closing state
        case WebSocket.CLOSED: // if the client is closed
        default: // if the client is not one of the above
          return "closed"; // return the closed state
      }
    } else {
      return "closed"; // return the closed state
    }
  }
}

//module.exports = AnotherTwitchPubSub; // export the pubsub class
window.AnotherTwitchPubSub = AnotherTwitchPubSub; // export the AnotherTwitchPubSub class as a global variable
